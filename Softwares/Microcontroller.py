import machine
import utime

############################### define functions #############################################################
def find_distance (trigger, echo):
    trigger.value(1)
    utime.sleep(0.00001)
    trigger.value(0)
    while echo.value() != 1:
        pass
    start = utime.time()
    while echo.value() == 1:
        pass
    end = utime.time()
    return (end-start)*34500/2

############################## define variables ############################################################
ultra1_trig = machine.Pin(2, machine.Pin.OUT)
ultra1_trig.value(0)
ultra1_echo = machine.Pin(3, machine.Pin.IN, machine.Pin.PULL_DOWN)
ultra2_trig = machine.Pin(0, machine.Pin.OUT)
ultra2_trig.value(0)
ultra2_echo = machine.Pin(1, machine.Pin.IN, machine.Pin.PULL_DOWN)
ultra3_trig = machine.Pin(22, machine.Pin.OUT)
ultra3_trig.value(0)
ultra3_echo = machine.Pin(21, machine.Pin.IN, machine.Pin.PULL_DOWN)
ultra4_trig = machine.Pin(26, machine.Pin.OUT)
ultra4_trig.value(0)
ultra4_echo = machine.Pin(25, machine.Pin.IN, machine.Pin.PULL_DOWN)
RP_out = machine.Pin(7, machine.Pin.OUT)
RP_out.value(0)
RP_in = machine.Pin(8, machine.Pin.IN, machine.Pin.PULL_DOWN)
control_left = machine.Pin(11, machine.Pin.OUT)
control_left.value(0)
control_right = machine.Pin(12, machine.Pin.OUT)
control_right.value(0)
sound_in = machine.Pin(16, machine.Pin.IN, machine.Pin.PULL_DOWN)
button = machine.Pin(13, machine.Pin.IN, machine.Pin.PULL_UP)

metal_detected = False
confirmation_state = True
err_code = 1000

operation_state = 0 # 0——operation holylight (wait mode), 1——operation starburst (normal mode)
                    # 2——operation swordfire (emergency mode), 3——operation darknight (wait mode #001 (state before wait mode or normal mode))
sub_state = 3 #sub-states of operation starburst and swordfire, 0——forward, 1——turn left, 2——turn right, 3——stop
binary_state = 1 #change between left/right default rotation in operation starburst
check_state = 0 #used to seperate distinct measurement between signals

f_counter = 0 #forward state accumulator
l_counter = 0 #turn left state accumulator
r_counter = 0 #turn right state accumulator
s_counter = 0 #stop state accumulator
w_counter = 0 #wait state accumulator

f_thre = 100000
t_thre = 500000

######################## main program——operation state control #####################################################
while True:
    if metal_detected == True:
        metal_detected = False
        operation_state = 3
    if operation_state == 0:
        control_left.value(0)
        control_right.value(0)
        print ("wait mode, press the button to continue")
        while button.value() == 1:
            pass
        operation_state = 1
        print ("button pressed, begin operation")
        utime.sleep(2)
    elif operation_state == 3:
        control_left.value(0)
        control_right.value(0)
        while RP_in != 1:
            w_counter += 1
            utime.sleep(0.25)
            if w_counter >= 160:
                confirmation_state = False
                break
        w_counter = 0
        RP_out.value(0)
        if confirmation_state == True:
            operation_state = 0
        else:
            operation_state = 1
            confirmation_state = True
    elif operation_state == 1:
        d1 = find_distance (ultra1_trig, ultra1_echo)
        d2 = find_distance (ultra2_trig, ultra2_echo)
        d3 = find_distance (ultra3_trig, ultra3_echo)
        if d1 < 10 or d2 < 10 or d3 < 10:
            operation_state = 0
        if sub_state == 0:
            if d1 > 50 and d2 > 20 and d3 > 20:
                operation_state = 1
                if d2 > 50 and d3 <= 50:
                    binary_state = 1
                elif d2 <= 50 and d3 > 50:
                    binary_state = 0
                else:
                    pass
            elif d1 > 50 and d2 <= 20 and d3 > 20:
                if d3 > 50:
                    binary_state = 0
                operation_state = 2
                err_code = 2111 #turn right in emergency mode by small amount
            elif d1 > 50 and d2 > 20 and d3 <= 20:
                if d2 > 50:
                    binary_state = 1
                operation_state = 2
                err_code = 2222 #turn left in emergency mode by small amount
            elif d1 <= 50 and d2 > 50 and d3 <= 50:
                operation_state = 2
                err_code = 2333 #turn left in emergency mode
                binary_state = 0
            elif d1 <= 50 and d2 <= 50 and d3 > 50:
                operation_state = 2
                err_code = 2555 #turn right in emergency mode
                binary_state = 1
            elif d1 <= 50 and d2 <= 50 and d3 <= 50:
                opration_state = 0 #If both sides do not have sufficient space, stop the machine
            elif d1 <= 50 and d2 > 50 and d3 > 50:
                operation_state = 2
                if binary_state == 1:
                    err_code = 2333 #If both sides have sufficient space, turn left to keep track of the motion
                    binary_state = 0
                else:
                    err_code = 2555
                    binary_state = 1
            else:
                operation_state = 0 #All other cases are too dangerous, thus, stops the machine
    else:
        d1 = find_distance (ultra1_trig, ultra1_echo)
        d2 = find_distance (ultra2_trig, ultra2_echo)
        d3 = find_distance (ultra3_trig, ultra3_echo)
        if d1 < 10 or d2 < 10 or d3 < 10:
            operation_state = 0
            
        
############################ Main Program——sub-state control ################################################################
    if operation_state == 1:
        if sub_state == 0:
            control_left.value(1)
            control_right.value(1)
            if f_counter <= f_thre:
                f_counter += 1
            else:
                if (binary_state == 1):
                    sub_state = 1
                else:
                    sub_state = 2
                f_counter = 0
        elif sub_state == 1:
            control_left.value(0)
            control_right.value(1)
            if l_counter <= t_thre:
                l_counter += 1
            else:
                sub_state = 0
                l_counter = 0
        elif sub_state == 2:
            control_left.value(1)
            control_right.value(0)
            if r_counter <= t_thre:
                r_counter += 1
            else:
                sub_state = 0
                r_counter = 0
        else:
            control_left.value(0)
            control_right.value(0)
            if err_code == 1000:
                sub_state = 0
                
    if operation_state == 2:
        if err_code == 2111:
            if r_counter <= t_thre/5:
                control_left.value(1)
                control_right.value(0)
                r_counter += 1
            else:
                control_left.value(0)
                control_right.value(1)
                if l_counter <= t_thre/5:
                    l_counter += 1
                else:
                    sub_state = 3
                    operaiton_state = 1
                    err_code = 1000
                    l_counter = 0
                    r_counter = 0
                    
        elif err_code == 2222:
            if l_counter <= t_thre/5:
                control_left.value(0)
                control_right.value(1)
                l_counter += 1
            else:
                control_left.value(1)
                control_right.value(0)
                if r_counter <= t_thre/5:
                    r_counter += 1
                else:
                    sub_state = 3
                    operaiton_state = 1
                    err_code = 1000
                    l_counter = 0
                    r_counter = 0
        elif err_code == 2333:
            control_left.value(0)
            control_right.value(1)
            if l_counter <= t_thre/2:
                l_counter += 1
            else:
                sub_state = 3
                operaiton_state = 1
                err_code = 1000
                l_counter = 0
        else:
            control_left.value(1)
            control_right.value(0)
            if r_counter <= t_thre/2:
                r_counter += 1
            else:
                sub_state = 3
                operaiton_state = 1
                err_code = 1000
                r_counter = 0
############################### signals update ########################################################
    if sound_in == 1 and check_state == 0:
        metal_detected == True
        RP_out.value(1)
        check_state = 1
    else:
        check_state = 0
        

        
        
    
        
        
        





