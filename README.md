# Automated metal detector robotics

This is a robotics project aiming to build an automated robot capable of metal detection and navigation within a specific area. Project is designed to satisfy daily life and industrial needs.

## Contents
This gitlab repository consists of 3 directories: lab notebook (Team member Jack Li), software (where all the software and codes we have used in this project are stored), and PCB (where the circuit schematic, PCB layout files are all included).

## Installation
For code testification with microcontroller using RP2040, download the code contained in Software including all the files. PCB files have been created to list all the schematic and PCB layout used for our project.

## Usage
For software usage, connect microcontroller to PC, and drop the uf2 file to the new detected device. This sets up microPython environment. Then open microcontroller.py and save it as main.py to the device which will begin functioning once it is disconnected from PC and powered from other external sources. This requires PCB connection.

## Support
For any question, reach out to shunjie2@illinois.edu

## Authors and acknowledgment
This repository is created by Jack Li with contents specifically related to UIUC ECE 445 project. Contents only include those done individually.
